#!/bin/bash

maxStudents=${MAXSTUDENTS:-24}

mkdir -p mariadb sqlitedb

>mariadb/initial-script.sql
for student in $(seq -w $maxStudents); do
   for db in {a..c}; do
      echo "create database if not exists db${student}_${db};" >> mariadb/initial-script.sql
   done
   echo "grant all on \`db${student}_%\`.* to 'db${student}'@'%' identified by '123';" >> mariadb/initial-script.sql

   touch sqlitedb/db${student}.sqlite3
done

chmod 777 sqlitedb
chmod 666 sqlitedb/db*.sqlite3
