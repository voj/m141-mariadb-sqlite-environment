#!/bin/bash
# 2023-05, lp5jvogel

export MAXSTUDENTS=30

apt -y install docker.io docker-compose

# get all files from gitlab
git clone https://gitlab.com/voj/m141-mariadb-sqlite-environment.git

# creates for each student maria-db: db01_a..db01_c and for each student a sqlite db
cd m141-mariadb-sqlite-environment
./create-dbs.sh

# start all services
docker-compose up -d

# creates the databases and set privileges
docker exec m141-mariadb-sqlite-environment_db_1 sh -c 'mariadb -uroot -pexample mysql < /var/lib/mysql/initial-script.sql'
